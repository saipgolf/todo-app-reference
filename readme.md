Install Docker and Fn on Linux, use this guide:
> https://github.com/fnproject/tutorials/blob/master/install/README.md

Deploy Redis with exposed port:
> sudo docker run --name redis -p 6379:6379 -d redis

Start Fn using:
> fn start --log-level DEBUG

Setup Fn to use local development environment:
> fn update context api-url http://localhost:8080
> fn update context registry localdev

To deploy app, use command in the todo-app folder:
> fn deploy --create-app --all --local

Deploy fnproject UI to docker:
> sudo docker run --link fnserver:api -p 4000:4000 -d --name fnui -e "FN_API_URL=http://api:8080" fnproject/ui

And then access the UI at http://localhost:4000, to verify that application is running.

See usage.txt for use cases.