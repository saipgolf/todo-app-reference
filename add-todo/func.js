const fdk=require('@fnproject/fdk');
const redis=require("redis");
const util=require('util');

// Expects an object with the property "user" and "todo":
// Example: {"user": "user1", "todo": "sample todo item"}
//
// Returns 200 OK if successful.
fdk.handle(function(input){
  if (!input) {
    throw new Error("input is not defined");
  }
  if (!input.todo) {
    throw new Error("input.todo is not defined");
  }
  if (!input.user) {
    throw new Error("input.user is not defined");
  }

  const client = redis.createClient({
    host: "172.17.0.1",
    port: 6379
  });

  const syncListPut = util.promisify(client.rpush).bind(client);

  return syncListPut(input.user, input.todo)
    .then(count => "");
})
