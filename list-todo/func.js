const fdk=require('@fnproject/fdk');
const redis=require("redis");
const util=require('util');
const url=require('url');

// Expects an a query param with user specified.
//
// Returns an object with the list of todo entries:
// Example: {"user": "user1", "todo": ["clean up", "make dinner"]}
fdk.handle(function(input, ctx){
  const requestURL = ctx.headers["Fn-Http-Request-Url"];
  if (!requestURL) {
    throw new Error("request URL is not defined is not defined");
  }
  const queryData = url.parse(requestURL[0], true).query;
  
  const user = queryData.user;
  if (!user) {
    throw new Error("query param user is not defined");
  }

  const client = redis.createClient({
    host: "172.17.0.1",
    port: 6379
  });

  const syncListGet = util.promisify(client.lrange).bind(client);

  return syncListGet(user, 0, 1000)
    .then(todoList => JSON.stringify({user: user, todo: todoList}));
})
