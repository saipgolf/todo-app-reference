const fdk=require('@fnproject/fdk');

// Expects no parameters.
//
// Returns an static string representation the authors of the applications.
fdk.handle(function(input){
  return "about-internal: Sample application created by Team Golf"
})
