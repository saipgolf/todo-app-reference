const fdk=require('@fnproject/fdk');
const redis=require("redis");
const util=require('util');

// Expects an object with the property "user":
// Example: {"user": "user1"}
//
// Returns 200 OK if successful.
fdk.handle(function(input){
  if (!input) {
    throw new Error("input is not defined");
  }
  if (!input.user) {
    throw new Error("input.user is not defined");
  }

  const client = redis.createClient({
    host: "172.17.0.1",
    port: 6379
  });

  const syncDelete = util.promisify(client.del).bind(client);

  return syncDelete(input.user)
    .then(_ => "");
})
