const fdk=require('@fnproject/fdk');
const fetch=require('node-fetch');

// Expects no parameters.
//
// Returns an static string representation the authors of the applications.
fdk.handle(function(input){
  const getUrl = "http://172.17.0.1:8080/t/todoapp/about-internal"

  return fetch(getUrl, 
    { 
      method: 'GET'
    })
    .then(res => res.json());
})
